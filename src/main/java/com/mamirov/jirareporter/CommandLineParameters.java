package com.mamirov.jirareporter;

import com.beust.jcommander.Parameter;

public class CommandLineParameters {
    @Parameter(
            names = {"--buildType", "-b"},
            description = "TeamCity build type"
    )
    private String buildType;

    @Parameter(
            names = {"--help", "-h"},
            help = true,
            description = "Help"
    )
    private boolean help;

    @Parameter(
            names = {"--progressIssue", "-p"},
            description = "Enable progress issue"
    )
    private boolean enableProgressIssue;

    @Parameter(
            names = {"--issue", "-i"},
            description = "JIRA Issue ID"
    )
    private String issueId;
    public String getBuildType(){
        return buildType;
    }

    public String getIssueId(){
        return issueId;
    }

    public boolean isHelp(){
        return help;
    }

    public boolean isEnableProgressIssue(){
        return enableProgressIssue;
    }
}
