package com.mamirov.jirareporter;

import com.atlassian.jira.rest.client.NullProgressMonitor;
import com.atlassian.jira.rest.client.domain.Comment;
import com.beust.jcommander.JCommander;
import com.mamirov.jirareporter.jira.JIRAConfig;
import com.mamirov.jirareporter.teamcity.TeamCityXMLParser;

import static com.mamirov.jirareporter.jira.JIRAClient.*;

public class Reporter {

    private static String issueId;

    private static CommandLineParameters cParams = new CommandLineParameters();

    public static void main(String [] args){
        JCommander jCommander = new JCommander(cParams, args);
        if(cParams.isHelp()){
            jCommander.usage();
            System.exit(0);
        }
        report();
        progressIssue();
    }

    public static String getBuildType(){
        return cParams.getBuildType();
    }

    public static String getIssueId(){
        String issueIdPlace = JIRAConfig.getIssuePlace();
        if(issueIdPlace == null || issueIdPlace.isEmpty()){
            System.out.println("Enter place for getting issue id in jira.properties\nExample: 'cmd', 'teamcity', 'custom'");
            System.exit(0);
        }
        switch (issueIdPlace){
            case "cmd":
                issueId = cParams.getIssueId();
                break;
            case "teamcity":
                issueId = TeamCityXMLParser.getIssue();
                break;
            case "custom":
                issueId = JIRAConfig.getIssueId();
                break;
        }
        issueIdAlert();
        return issueId;
    }

    private static void report(){
        System.out.println("ISSUE: "+getIssueId());
        System.out.println("Title: "+getIssue().getSummary());
        System.out.println("Description: "+getIssue().getDescription());
        NullProgressMonitor pm = new NullProgressMonitor();
        getRestClient().getIssueClient().addComment(pm, getIssue().getCommentsUri(), Comment.valueOf(LocalConfig.getTestResultText()));
    }

    private static void progressIssue(){
        NullProgressMonitor pm = new NullProgressMonitor();
        if(JIRAConfig.issueProgressingIsEnable() || cParams.isEnableProgressIssue()){
            String teamCityBuildStatus = TeamCityXMLParser.getStatusBuild();
            getRestClient().getIssueClient().transition(getIssue().getTransitionsUri(), getTransitionInput(JIRAConfig.prepareJiraWorkflow(teamCityBuildStatus).get(getIssueStatus())), pm);
        }
    }

    private static void issueIdAlert(){
        if(issueId == null || issueId.isEmpty()){
            System.out.println("Issue id is empty");
            System.exit(0);
        }
    }
}
